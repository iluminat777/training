
/*
  Задание:
  1. При помощи методов изложеных ниже, переформатировать ITEA_COURSES в массив который содержит длину строк каждого из элементов.
  2. Самостоятельно изучить метод Array.sort. Отфильтровать массив ITEA_COURSES по алфавиту.
      + Бонусный бал. Вывести на страничку списком
  3. Реализация функции поиска по массиву ITEA_COURSES.
      + Бонусный бал. Вывести на страничку инпут и кнопку по которой будет срабатывать поиск.

*/

const ITEA_COURSES = ["Курс HTML & CSS", "JavaScript базовый курс", "JavaScript продвинутый курс", "JavaScript Professional", "Angular 2.4 (базовый)", "Angular 2.4 (продвинутый)", "React.js", "React Native", "Node.js", "Vue.js"];
const button = document.querySelector('.btn-secondary');

function listArray () {
    for (var i = 0; i < ITEA_COURSES.length; i++) {
        let newlist = document.createElement("li");
        let lengthArray = document.createElement("span");
        newlist.innerHTML = ITEA_COURSES[i];
        lengthArray.innerHTML = ITEA_COURSES[i].length;
        document.getElementById("list_array").appendChild(newlist).appendChild(lengthArray);
    }

    var LogName = item => console.log( 'length:', item.length )
    ITEA_COURSES.forEach( LogName );
}

listArray();

function listArrayAB () {

    let list = ITEA_COURSES.map(function (e, i) {
        return {index: i, value: e.toLowerCase()};
    });

    list.sort(function (a, b) {
        return +(a.value > b.value) || +(a.value === b.value) - 1;
    });

    let result = list.map(function (e) {
        return ITEA_COURSES[e.index];
    });

    for (var i = 0; i < result.length; i++) {
        var newlist = document.createElement("li");
        newlist.innerHTML = result[i];
        document.getElementById("list_arrayAB").appendChild(newlist);
    }
}

listArrayAB();

button.addEventListener ("click",  function(e) {
    // var target = e && e.target || event.srcElement;
    var input = document.getElementById('text').value;

    function find(array, value) {
        if (array.indexOf) {
            return array.indexOf(value);
        }

        for (var i = 0; i < array.length; i++) {
            if (array[i] === value) return i;
        }

        return -1;
    }

    let index = find(ITEA_COURSES, input);

    alert( index );
});

